Brand Monitoring Scraper
========================


Minimum requirement
-------------------

- Python 3.4
- Java 1.8.0_73
- Elasticsearch 5.0.1
- Redis 2.8.4
- Selenium Standalone Server + ChromeDriver


Scraper credentials
-------------------

Twitter API key
~~~~~~~~~~~~~~~

To get the twitter API key please follow the steps documented in https://dev.twitter.com/oauth/overview/application-owner-access-tokens

Set the `TWITTER_CONSUMER_KEY` and `TWITTER_CONSUMER_SECRET` env variables with the access token and access token secret generated from the Twitter applications registration process.

Facebook Account
~~~~~~~~~~~~~~~~

Create a facebook account and set the `FACEBOOK_USER` and `FACEBOOK_PASSWORD` with the email and password used for the account.

For testing purpose, the application require `FACEBOOK_TEST_USER` and `FACEBOOK_TEST_PASSWORD` to be set. This parameters can be filled with the same account used for the live environment or with a different account specific for testing purpose only.


Development notes
-----------------

Python
~~~~~~

Install project dependencies using pip::

    $ pip install -r requirements.txt

Setup environment variable by copying the `.env.example` file to `.env` file and edit the content to match your system::

    $ cp .env.example .env

Make sure elasticsearch and redis server is running and run celery worker and tasks scheduler::

    $ celery -A tasks worker
    $ celery -A tasks beat


Testing
~~~~~~~

To run the tests, first make sure the `DB_TEST_HOST`, `DB_TEST_DATABASE`, `DB_TEST_USERNAME`, and `DB_TEST_PASSWORD` variable in `.env` file is set properly.

Run code checker::

    $ flake8

Run the test runner::

    $ pytest

Or execute it in one line::

    $ flake8 && pytest

For facebook scraper, the tests required to run a selenium instance. This process will make the tests run slower, therefore it is skipped by default and require special switch to run it::

    $ pytest --selenium

In deployment pipeline, it is a mandatory to run all the tests and the code checker by executing these command::

    $ flake8 && pytest --selenium


Proxy request
~~~~~~~~~~~~~

To use proxy for scraping the web, create `proxies.txt` in project root directroty. Fill the file with list of proxy address seperated by new line. The application will then randomly pick one proxy from the list to be used on each request.

Proxy list file content example::

    37.187.100.23:3128
    70.248.28.23:800
    200.68.27.100:3128
    118.165.132.241:8998
    94.23.249.218:3128
    94.177.179.139:666
    94.177.160.72:65000

Or you can customize the file name and location by setting the `PROXY_LIST` variable in `.env` file.
