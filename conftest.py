def pytest_addoption(parser):
    parser.addoption(
            '--selenium',
            action='store_true',
            help='include tests with selenium requirement')
