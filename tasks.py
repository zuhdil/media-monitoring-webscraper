import logging
from os import getenv, path
from random import choice
from datetime import datetime, timedelta

from celery import Celery
from elasticsearch import Elasticsearch
from requests import Session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from dotenv import load_dotenv, find_dotenv

from webscraper.configurations import ConfigRepository
from webscraper.scraper import Scraper, Indexer, TZ
from webscraper.google_driver import ScrapeDriver as GoogleScraper
from webscraper.bing_driver import ScrapeDriver as BingScraper
from webscraper.twitter_driver import (
        TwitterAPI,
        ScrapeDriver as TwitterScraper)
from selenium import webdriver
from webscraper.facebook_driver import ScrapeDriver as FacebookScraper
from webscraper.kaskus_driver import ScrapeDriver as KaskusScraper
from webscraper.detik_driver import ScrapeDriver as DetikScraper
from play_scraper.scraper import PlayScraper
from webscraper.play_driver import ScrapeDriver as PlayScrapeDriver


load_dotenv(find_dotenv())

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

app = Celery(
        broker=getenv('CELERY_BROKER_URL', 'redis://localhost:6379/0'))
app.conf.beat_schedule = {
        'run-google-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=60),
            'args': (['google'])},
        'run-bing-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=60),
            'args': (['bing'])},
        'run-twitter-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=60),
            'args': (['twitter'])},
        'run-facebook-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=180),
            'args': (['facebook'])},
        'run-kaskus-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=60),
            'args': (['kaskus'])},
        'run-detik-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=60),
            'args': (['detik'])},
        'run-play-scraper': {
            'task': 'tasks.run_scraper',
            'schedule': timedelta(minutes=720),
            'args': (['play'])}}

database_config = 'mysql+pymysql://{user}:{password}@{host}/{database}'.format(
        user=getenv('DB_USER'),
        password=getenv('DB_PASSWORD'),
        host=getenv('DB_HOST'),
        database=getenv('DB_DATABASE'))
connection = create_engine(getenv('DB_CONNECTION', database_config))
DBSession = scoped_session(sessionmaker(bind=connection))

scraper_index = getenv(
        'SCRAPER_ELASTICSEARCH_INDEX', 'brand-monitoring')
elasticsearch_hosts = getenv(
        'ELASTICSEARCH_HOSTS', 'localhost:9200').split(',')
indexer = Indexer(Elasticsearch(elasticsearch_hosts), scraper_index)


def load_proxies(proxies_file):
    if path.isfile(proxies_file):
        with open(proxies_file) as file:
            return [
                    'http://' + item
                    for item
                    in file.read().splitlines()
                    if item]

    return []


def make_google_driver(proxy=None):
    return GoogleScraper(Session(), proxy=proxy)


def make_bing_driver(proxy=None):
    return BingScraper(Session(), proxy=proxy)


def make_twitter_driver(proxy=None):
    twitter_api = TwitterAPI(
            getenv('TWITTER_CONSUMER_KEY'),
            getenv('TWITTER_CONSUMER_SECRET'),
            proxy=proxy)

    return TwitterScraper(twitter_api)


def make_facebook_driver(proxy=None):
    user = getenv('FACEBOOK_USER')
    password = getenv('FACEBOOK_PASSWORD')
    if not user or not password:
        raise RuntimeError(
            'Facebook scraper require facebook user and password credentials')

    command_executor = getenv(
            'SELENIUM_COMMAND_EXECUTOR', 'http://127.0.0.1:4444/wd/hub')

    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    options.add_argument('--disable-notifications')

    driver = webdriver.Remote(
       command_executor=command_executor,
       desired_capabilities=options.to_capabilities())

    return FacebookScraper(driver, user, password)


def make_kaskus_driver(proxy=None):
    return KaskusScraper(Session(), proxy=proxy)


def make_detik_driver(proxy=None):
    return DetikScraper(Session(), proxy=proxy)


def make_play_driver(proxy=None):
    return PlayScrapeDriver(PlayScraper())


available_drivers = {
        'google': make_google_driver,
        'bing': make_bing_driver,
        'twitter': make_twitter_driver,
        'facebook': make_facebook_driver,
        'kaskus': make_kaskus_driver,
        'detik': make_detik_driver,
        'play': make_play_driver}


def get_drivers(selected, proxy=None):
    drivers = []

    for name in available_drivers.keys():
        if name in selected:
            make_driver = available_drivers[name]
            driver = make_driver(proxy)
            drivers.append(driver)

    return drivers


@app.task
def run_scraper(scrapers=[]):
    proxies = load_proxies(getenv('PROXY_LIST', 'proxies.txt'))
    db = DBSession()
    repository = ConfigRepository(db)

    try:
        for config in repository.get_active(datetime.now(TZ)):
            proxy = choice(proxies) if proxies else None
            drivers = get_drivers(scrapers, proxy)
            scraper = Scraper(config.client_id, drivers, indexer)

            logger.info('scrape "{}" for {}'.format(
                config.keyword, config.client_id))
            try:
                scraper.scrape(config.keyword)
            except Exception as e:
                logger.exception(e)
            finally:
                for driver in drivers:
                    close_driver = getattr(driver, 'close', None)
                    if callable(close_driver):
                        close_driver()
    finally:
        db.close()
