import pytest
from unittest.mock import Mock, call
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from webscraper.pageobject import Find, PageObject, WrapElement


def test_init_with_no_locator_should_throws_error():
    with pytest.raises(ValueError):
        Find()


@pytest.mark.parametrize('locator, expected', [
    ({'css': 'foo'}, (By.CSS_SELECTOR, 'foo')),
    ({'id': 'foo'}, (By.ID, 'foo')),
    ({'name': 'foo'}, (By.NAME, 'foo')),
    ({'xpath': 'foo'}, (By.XPATH, 'foo')),
    ({'link_text': 'foo'}, (By.LINK_TEXT, 'foo')),
    ({'partial_link_text': 'foo'}, (By.PARTIAL_LINK_TEXT, 'foo')),
    ({'tag_name': 'foo'}, (By.TAG_NAME, 'foo')),
    ({'class_name': 'foo'}, (By.CLASS_NAME, 'foo'))])
def test_init_with_valid_locator(locator, expected):
    el = Find(**locator)
    assert el.locator == expected


def test_init_with_ambiguous_locator_should_throws_error():
    with pytest.raises(ValueError):
        Find(id='id', name='foo')


def test_init_with_invalid_locator_should_throws_error():
    with pytest.raises(ValueError):
        Find(invalid='foo')


def test_invalid_wrap_should_throws_error():
    with pytest.raises(ValueError):
        Find(Mock, tag_name='tag')


class FakeWrap(WrapElement):
    pass


class FakePage(PageObject):
    elem = Find(css='foo')
    wrap = Find(FakeWrap, css='wrap')


@pytest.fixture
def webdriver():
    return Mock(WebDriver)


@pytest.fixture
def webelement():
    return Mock(WebElement)


def test_get_as_descriptor_call_driver_find_element(webdriver):
    page = FakePage(webdriver)
    webdriver.find_element.return_value = 'XXX'
    assert page.elem == 'XXX'
    assert webdriver.find_element.mock_calls == [call(By.CSS_SELECTOR, 'foo')]


def test_get_not_found_should_return_none(webdriver):
    page = FakePage(webdriver)
    webdriver.find_element.side_effect = NoSuchElementException
    assert page.elem is None


def test_get_not_as_descriptor_should_return_it_self():
    assert type(FakePage.elem) == Find
    assert FakePage.elem.locator == (By.CSS_SELECTOR, 'foo')


def test_get_with_wrap_element(webdriver, webelement):
    page = FakePage(webdriver)
    webdriver.find_element.return_value = webelement
    assert isinstance(page.wrap, FakeWrap)
