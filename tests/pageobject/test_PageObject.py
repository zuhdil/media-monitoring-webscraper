import pytest
from unittest.mock import Mock, PropertyMock, call
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from webscraper.pageobject import PageObject


@pytest.fixture
def webdriver():
    return Mock(WebDriver)


def test_driver_return_webdriver(webdriver):
    page = PageObject(webdriver)
    assert page.driver is webdriver


def test_find_element_should_delegate_to_webdriver(webdriver):
    webdriver.find_element.return_value = 'XXX'
    page = PageObject(webdriver)
    result = page.find_element(By.ID, 'foo')
    assert result == 'XXX'
    assert webdriver.find_element.mock_calls == [call(By.ID, 'foo')]


def test_find_elements_should_delegate_to_webdriver(webdriver):
    webdriver.find_elements.return_value = 'XXX'
    page = PageObject(webdriver)
    result = page.find_elements(By.ID, 'foo')
    assert result == 'XXX'
    assert webdriver.find_elements.mock_calls == [call(By.ID, 'foo')]


def test_init_with_url_parameter():
    page = PageObject(None, 'http://localhost')
    assert page.url == 'http://localhost'


class FakePage(PageObject):
    URL = 'http://localhost'


def test_init_with_url_from_subclass_URL_attribute():
    page = FakePage(None)
    assert page.url == 'http://localhost'


def test_init_with_url_parameter_override_subclass_URL_attribute():
    page = FakePage(None, 'http://override')
    assert page.url == 'http://override'


def test_open_should_call_webdriver_get_method(webdriver):
    page = FakePage(webdriver)
    page.open()
    assert webdriver.get.mock_calls == [call('http://localhost')]


def test_open_should_return_self_instance(webdriver):
    page = FakePage(webdriver)
    assert page.open() is page


def test_goto_should_call_webdirver_with_new_url(webdriver):
    page = PageObject(webdriver, 'http://localhost/foo')
    page.goto('/bar')
    assert webdriver.get.mock_calls == [call('http://localhost/bar')]


def test_goto_should_return_new_PageObject_with_the_current_url(webdriver):
    type(webdriver).current_url = PropertyMock(
            return_value='http://localhost/bar')
    page = PageObject(webdriver, 'http://localhost/foo')
    target = page.goto('/bar')
    assert target is not page
    assert isinstance(target, PageObject)
    assert target.url == 'http://localhost/bar'
