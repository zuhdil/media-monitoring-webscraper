import pytest
from unittest.mock import Mock
from selenium.webdriver.remote.webelement import WebElement
from webscraper.pageobject import WrapElement


@pytest.fixture
def webelement():
    return Mock(WebElement)


def test_init_with_invalid_argument_should_throws_error():
    with pytest.raises(ValueError):
        WrapElement({})


def test_delegate_method_call_to_wrapped_element(webelement):
    wrap = WrapElement(webelement)
    wrap.get_attribute('name')
    assert webelement.get_attribute.called is True
