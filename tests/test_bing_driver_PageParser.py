import pytest
from webscraper.bing_driver import PageParser


@pytest.fixture
def first_page():
    with open('tests/fixtures/bing-search-first-page.html', 'r') as page:
        return PageParser(page.read())


@pytest.fixture
def second_page():
    with open('tests/fixtures/bing-search-next-page.html', 'r') as page:
        return PageParser(page.read())


@pytest.fixture
def last_page():
    with open('tests/fixtures/bing-search-last-page.html', 'r') as page:
        return PageParser(page.read())


def test_items_return_list_of_scrape_result_dict(first_page):
    assert len([item for item in first_page.items]) == 20


def test_result_item_title_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['title'] == (
            'Public Service Announcement - Honesty by Public Relations ...')


def test_result_item_url_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['url'] == 'https://www.youtube.com/watch?v=IbTh389faj8'


def test_result_item_snippet_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['snippet'] == (
            'What do PR professionals expect from their media monitoring ?'
            ' ... Indonesia Tourism MKT 13-9C Explore Indonesia 2012 '
            'Intercultural Communication …')


def test_result_rich_item_snippet_has_correct_value(first_page):
    rich_item = ([item for item in first_page.items])[3]
    assert rich_item['snippet'] == (
            'Pidie Disaster Mitigation Agency head Apriadi said his agency '
            'had been monitoring the earthquake, ... Indonesia advances to '
            'AFF Cup finals ; ... Media …')


def test_result_tweet_item_snippet_has_correct_value(first_page):
    tweet_item = ([item for item in first_page.items])[5]
    assert tweet_item['snippet'] == (
            'Sachmal kennt ihr ein gutes Tool für Media Monitoring ? ... '
            'Indonesia: 89887: AXIS, 3, Telkomsel, Indosat, XL Axiata: '
            'Italy: 4880804: Wind: 3424486444 ...')


def test_is_last_on_first_page_should_return_false(first_page):
    assert first_page.is_last is False


def test_is_last_on_second_page_should_return_false(second_page):
    assert second_page.is_last is False


def test_is_last_on_last_page_should_return_true(last_page):
    assert last_page.is_last is True
