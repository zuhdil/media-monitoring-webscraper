import pytest
from unittest.mock import Mock, PropertyMock
from requests import Session
from webscraper.bing_driver import PageRequest, PageParser


@pytest.fixture
def page_text():
    with open("tests/fixtures/bing-search-last-page.html", "r") as page:
        return page.read()


@pytest.fixture
def session(page_text):
    session = Mock(Session)
    type(session.get.return_value).text = PropertyMock(return_value=page_text)
    return session


def test_should_make_get_request_with_bing_search_url(session):
    page_request = PageRequest(session, 'testing')
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 100,
                'first': 0,
                'FORM': 'PERE'},
            proxies=None)


def test_can_specify_result_per_page_number(session):
    page_request = PageRequest(session, 'testing', perpage=10)
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 10,
                'first': 0,
                'FORM': 'PERE'},
            proxies=None)


def test_result_per_page_number_ignore_number_more_than_100(session):
    page_request = PageRequest(session, 'testing', perpage=110)
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 100,
                'first': 0,
                'FORM': 'PERE'},
            proxies=None)


def test_result_per_page_number_ignore_number_lower_than_1(session):
    page_request = PageRequest(session, 'testing', perpage=-1)
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 100,
                'first': 0,
                'FORM': 'PERE'},
            proxies=None)


def test_should_increment_result_per_page_offset(session):
    page_request = PageRequest(session, 'testing')
    page_request.get_page(2)
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 100,
                'first': 100,
                'FORM': 'PERE'},
            proxies=None)


def test_get_page_should_return_PageParser_object(session):
    page_request = PageRequest(session, 'testing')
    page = page_request.get_page()
    assert type(page) == PageParser


def test_should_be_able_to_use_proxy_for_request_call(session):
    proxy = 'http://127.0.0.1:8080'
    page_request = PageRequest(session, 'testing', proxy=proxy)
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.bing.com/search',
            params={
                'q': 'testing',
                'filters': 'ex1:"ez1"',
                'count': 100,
                'first': 0,
                'FORM': 'PERE'},
            proxies={'http': proxy})
