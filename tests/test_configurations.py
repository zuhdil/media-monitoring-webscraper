import pytest
from os import getenv
from uuid import uuid4
from datetime import date, datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from factory import Factory, Sequence, fuzzy
from webscraper.configurations import (
        Client, MonitoringConfig, ConfigRepository)


class ClientFactory(Factory):
    class Meta:
        model = Client

    id = Sequence(lambda x: str(uuid4()))
    name = fuzzy.FuzzyText()
    username = fuzzy.FuzzyText()
    password = fuzzy.FuzzyText()
    remember_token = fuzzy.FuzzyText()
    created_at = fuzzy.FuzzyDate(date(2016, 1, 1))
    updated_at = fuzzy.FuzzyDate(date(2016, 1, 1))
    is_active = True


class MonitoringConfigFactory(Factory):
    class Meta:
        model = MonitoringConfig

    id = Sequence(lambda x: str(uuid4()))
    keyword = fuzzy.FuzzyText()
    started_at = fuzzy.FuzzyDate(date(2016, 1, 1))
    created_at = fuzzy.FuzzyDate(date(2016, 1, 1))
    updated_at = fuzzy.FuzzyDate(date(2016, 1, 1))


@pytest.fixture(scope='session')
def engine():
    config = 'mysql+pymysql://{user}:{password}@{host}/{database}'.format(
            user=getenv('DB_TEST_USER'),
            password=getenv('DB_TEST_PASSWORD'),
            host=getenv('DB_TEST_HOST'),
            database=getenv('DB_TEST_DATABASE'))

    return create_engine(getenv('DB_TEST_CONNECTION', config))


@pytest.fixture
def session(engine):
    Session = scoped_session(sessionmaker(bind=engine))
    session = Session()
    yield session
    session.rollback()
    session.close()


@pytest.fixture
def repository(session):
    return ConfigRepository(session)


def test_get_active_returns_all_active_config(repository, session):
    c1 = ClientFactory()
    c2 = ClientFactory()
    mc1 = MonitoringConfigFactory(started_at=datetime(2016, 11, 1))
    mc2 = MonitoringConfigFactory(started_at=datetime(2016, 11, 1))
    c1.monitoring_configs.append(mc1)
    c2.monitoring_configs.append(mc2)
    session.add_all([c1, c2])

    assert set([mc1, mc2]) == set(
            repository.get_active(datetime(2016, 11, 10)))


def test_get_active_client_with_multiple_config(repository, session):
    c1 = ClientFactory()
    mc1 = MonitoringConfigFactory(started_at=datetime(2016, 11, 1))
    mc2 = MonitoringConfigFactory(started_at=datetime(2016, 11, 10))
    mc3 = MonitoringConfigFactory(started_at=datetime(2016, 11, 20))
    c1.monitoring_configs.extend([mc1, mc2, mc3])
    session.add(c1)

    assert set([mc2]) == set(repository.get_active(datetime(2016, 11, 15)))


def test_get_active_with_multiple_clients_and_configs(repository, session):
    c1 = ClientFactory()
    mc11 = MonitoringConfigFactory(started_at=datetime(2016, 11, 10))
    mc12 = MonitoringConfigFactory(started_at=datetime(2016, 11, 15))
    mc13 = MonitoringConfigFactory(started_at=datetime(2016, 11, 20))
    c1.monitoring_configs.extend([mc11, mc12, mc13])

    c2 = ClientFactory()
    mc21 = MonitoringConfigFactory(started_at=datetime(2016, 11, 1))
    c2.monitoring_configs.append(mc21)

    c3 = ClientFactory()
    mc31 = MonitoringConfigFactory(started_at=datetime(2016, 11, 15))
    mc32 = MonitoringConfigFactory(started_at=datetime(2016, 11, 25))
    c3.monitoring_configs.extend([mc31, mc32])

    session.add_all([c1, c2, c3])

    assert set([mc11, mc21]) == set(
            repository.get_active(datetime(2016, 11, 11)))


def test_get_active_only_return_configs_for_active_clients(
            repository, session):
    c1 = ClientFactory()
    mc11 = MonitoringConfigFactory(started_at=datetime(2016, 11, 10))
    c1.monitoring_configs.append(mc11)

    c2 = ClientFactory(is_active=False)
    mc21 = MonitoringConfigFactory(started_at=datetime(2016, 11, 1))
    c2.monitoring_configs.append(mc21)

    c3 = ClientFactory()
    mc31 = MonitoringConfigFactory(started_at=datetime(2016, 11, 11))
    c3.monitoring_configs.append(mc31)

    session.add_all([c1, c2, c3])

    assert set([mc11, mc31]) == set(
            repository.get_active(datetime(2016, 11, 11)))
