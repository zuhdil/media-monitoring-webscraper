import pytest
from webscraper.detik_driver import PageParser


@pytest.fixture
def first_page():
    with open('tests/fixtures/detik-search-first-page.html', 'r') as page:
        return PageParser(page.read().encode('utf-8'))


@pytest.fixture
def last_page():
    with open('tests/fixtures/detik-search-last-page.html', 'r') as page:
        return PageParser(page.read().encode('utf-8'))


def test_items_return_list_of_scrape_result_dic(first_page):
    assert len([item for item in first_page.items]) == 10


def test_result_item_title_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['title'] == (
            "Bus Trans Semarang Dipasangi 'Telolet' Agar Menarik Penumpang")


def test_result_item_url_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['url'] == (
            'http://news.detik.com/read/2017/01/07/220943/3390310/10/'
            'bus-trans-semarang-dipasangi-telolet-agar-menarik-penumpang')


def test_result_item_snippet_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['snippet'] == (
            "Bus Trans Semarang akan dilengkapi klakson 'telolet' "
            "untuk menarik minat penumpang....")


def test_is_last_on_first_page_should_return_false(first_page):
    assert first_page.is_last is False


def test_is_last_on_last_page_should_return_true(last_page):
    assert last_page.is_last is True
