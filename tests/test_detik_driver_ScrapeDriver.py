import pytest
from datetime import datetime
from unittest.mock import Mock, PropertyMock, patch, call
from requests import Session
from webscraper.scraper import FetchResult
from webscraper.detik_driver import ScrapeDriver


@pytest.fixture
def first_page():
    with open('tests/fixtures/detik-search-first-page.html') as page:
        return page.read()


@pytest.fixture
def last_page():
    with open('tests/fixtures/detik-search-last-page.html') as page:
        return page.read()


@pytest.fixture
def session(last_page):
    session = Mock(Session)
    type(session.get.return_value).text = PropertyMock(return_value=last_page)
    return session


@pytest.fixture
def search_date():
    return datetime(2017, 1, 1)


@pytest.fixture
def driver(session, search_date):
    return ScrapeDriver(session, date=search_date)


def test_should_return_list_of_FetchResult(driver):
    first_item = next(driver.fetch('testing'))
    assert (
            type(first_item) == FetchResult
            and first_item.source == driver.source)


@patch('webscraper.detik_driver.time')
def test_should_request_for_next_page_if_current_page_is_not_last(
        _, driver, session, first_page, last_page):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page, last_page])
    [item for item in driver.fetch('testing')]
    assert session.get.call_count == 2


@patch('webscraper.detik_driver.time')
def test_should_request_for_next_page_with_correct_page_param(
        _, driver, session, first_page, last_page, search_date):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page, last_page])
    [item for item in driver.fetch('testing')]
    session.get.assert_called_with(
            'http://search.detik.com/search',
            params={
                'query': 'testing',
                'sortby': 'time',
                'fromdatex': search_date.strftime('%d/%m/%Y'),
                'todatex': search_date.strftime('%d/%m/%Y'),
                'page': 2},
            proxies=None)


@patch('webscraper.detik_driver.time')
def test_should_fetch_all_item(_, driver, session, first_page, last_page):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page, last_page])
    items = [item for item in driver.fetch('testing')]
    assert len(items) == 14


@patch('webscraper.detik_driver.time')
def test_should_sleep_between_requests(
        fake_time, driver, session, first_page, last_page):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page, last_page])
    [item for item in driver.fetch('testing')]
    assert fake_time.sleep.called


@patch('webscraper.detik_driver.time')
def test_only_sleep_if_more_than_one_request(fake_time, driver):
    [item for item in driver.fetch('testing')]
    assert not fake_time.sleep.called


@patch('webscraper.detik_driver.time')
def test_can_specify_sleep_time(fake_time, session, first_page, last_page):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page, last_page])
    sleep_time = 10.0
    driver = ScrapeDriver(session, sleep=sleep_time)
    [item for item in driver.fetch('testing')]
    assert fake_time.sleep.call_args == call(sleep_time)


def test_should_be_callable(driver):
    items = [item for item in driver('testing')]
    assert len(items) == 4


def test_should_be_able_to_use_proxy_for_request_call(session, search_date):
    proxy = 'http://127.0.0.1:8080'
    driver = ScrapeDriver(session, date=search_date, proxy=proxy)
    next(driver.fetch('foo'))
    session.get.assert_called_with(
            'http://search.detik.com/search',
            params={
                'query': 'foo',
                'sortby': 'time',
                'fromdatex': search_date.strftime('%d/%m/%Y'),
                'todatex': search_date.strftime('%d/%m/%Y')},
            proxies={'http': proxy})
