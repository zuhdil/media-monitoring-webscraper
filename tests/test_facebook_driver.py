import pytest
from os import getenv
from selenium import webdriver
from webscraper.facebook_driver import ScrapeDriver


FACEBOOK_USER = getenv('FACEBOOK_TEST_USER')
FACEBOOK_PASSWORD = getenv('FACEBOOK_TEST_PASSWORD')


@pytest.fixture
def driver():
    command_executor = getenv(
            'SELENIUM_COMMAND_EXECUTOR', 'http://127.0.0.1:4444/wd/hub')

    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    options.add_argument('--disable-notifications')

    return webdriver.Remote(
       command_executor=command_executor,
       desired_capabilities=options.to_capabilities())


@pytest.fixture
def scraper(driver):
    scraper = ScrapeDriver(driver, FACEBOOK_USER, FACEBOOK_PASSWORD)
    yield scraper
    scraper.close()


@pytest.mark.skipif(
        not pytest.config.getoption('--selenium'),
        reason='need --selenium option to run')
@pytest.mark.skipif(
        not FACEBOOK_USER or not FACEBOOK_PASSWORD,
        reason='require facebook user and password credential')
def test_run_scraper(scraper):
    items = [item for item in scraper('facebook', 1)]
    assert len(items) > 0
