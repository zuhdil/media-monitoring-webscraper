import pytest
from webscraper.google_driver import PageParser


@pytest.fixture
def first_page():
    with open('tests/fixtures/google-search-first-page.html', 'r') as page:
        return PageParser(page.read())


@pytest.fixture
def second_page():
    with open('tests/fixtures/google-search-next-page.html', 'r') as page:
        return PageParser(page.read())


@pytest.fixture
def last_page():
    with open('tests/fixtures/google-search-last-page.html', 'r') as page:
        return PageParser(page.read())


def test_items_return_list_of_ccrape_result_dict(first_page):
    assert len([item for item in first_page.items]) == 10


def test_result_item_title_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['title'] == 'Trends in Indonesia - Trendsmap'


def test_result_item_url_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['url'] == 'https://www.trendsmap.com/local/indonesia'


def test_result_item_snippet_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['snippet'] == (
        'Ayo dukung @melodyJKT48 dalam Indonesian Social Media Awards 2016 '
        '\\ndengan ..... Trending Topics del martes 28 de junio en Indonesia '
        '| Trendinalia.')


def test_is_last_on_first_page_should_return_false(first_page):
    assert first_page.is_last is False


def test_is_last_on_secon_page_should_return_false(second_page):
    assert second_page.is_last is False


def test_is_last_on_last_page_should_return_true(last_page):
    assert last_page.is_last is True
