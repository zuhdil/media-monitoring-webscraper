import pytest
from unittest.mock import Mock, PropertyMock, patch, call
from requests import Session
from webscraper.scraper import FetchResult
from webscraper.google_driver import ScrapeDriver


@pytest.fixture
def first_page_text():
    with open("tests/fixtures/google-search-first-page.html", "r") as page:
        return page.read()


@pytest.fixture
def last_page_text():
    with open("tests/fixtures/google-search-last-page.html", "r") as page:
        return page.read()


@pytest.fixture
def session(last_page_text):
    session = Mock(Session)
    type(session.get.return_value).text = PropertyMock(
            return_value=last_page_text)
    return session


@pytest.fixture
def driver(session):
    return ScrapeDriver(session)


def test_can_specify_result_per_page_number(session, driver):
    next(driver.fetch('testing', perpage=10))
    session.get.assert_called_with(
            'https://www.google.com/search',
            params={
                'q': 'testing',
                'hl': 'id',
                'btnG': 'Google Search',
                'tbs': 'qdr:h',
                'safe': 'off',
                'num': 10,
                'start': 0},
            proxies=None)


def test_should_return_list_of_FetchResult(driver):
    first_item = next(driver.fetch('testing'))
    assert (
            type(first_item) == FetchResult
            and first_item.source == driver.source)


@patch('webscraper.google_driver.time')
def test_should_request_for_next_page_if_current_page_is_not_last(
        _, driver, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    [item for item in driver.fetch('testing')]
    assert session.get.call_count == 2


@patch('webscraper.google_driver.time')
def test_should_request_for_next_page_with_correct_offset_params(
        _, driver, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    [item for item in driver.fetch('foo')]
    session.get.assert_called_with(
            'https://www.google.com/search',
            params={
                'q': 'foo',
                'hl': 'id',
                'btnG': 'Google Search',
                'tbs': 'qdr:h',
                'safe': 'off',
                'num': 100,
                'start': 100},
            proxies=None)


@patch('webscraper.google_driver.time')
def test_should_fetch_all_item(
        _, driver, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    items = [item for item in driver.fetch('testing')]
    assert len(items) == 18


@patch('webscraper.google_driver.time')
def test_should_sleep_between_requests(
        fake_time, driver, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    [item for item in driver.fetch('testing')]
    assert fake_time.sleep.called


@patch('webscraper.google_driver.time')
def test_only_sleep_if_more_than_one_request(fake_time, driver):
    [item for item in driver.fetch('testing')]
    assert not fake_time.sleep.called


@patch('webscraper.google_driver.time')
def test_can_specify_sleep_time(
        fake_time, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    sleep_time = 10.0
    driver = ScrapeDriver(session, sleep_time)
    [item for item in driver.fetch('testing')]
    assert fake_time.sleep.call_args == call(sleep_time)


@patch('webscraper.google_driver.time')
def test_should_be_callable(
        _, driver, session, first_page_text, last_page_text):
    type(session.get.return_value).text = PropertyMock(
            side_effect=[first_page_text, last_page_text])
    items = [item for item in driver('testing')]
    assert len(items) == 18


def test_should_be_able_to_use_proxy_for_request_call(driver, session):
    proxy = 'http://127.0.0.1:8080'
    driver = ScrapeDriver(session, proxy=proxy)
    next(driver.fetch('testing'))
    session.get.assert_called_with(
            'https://www.google.com/search',
            params={
                'q': 'testing',
                'hl': 'id',
                'btnG': 'Google Search',
                'tbs': 'qdr:h',
                'safe': 'off',
                'num': 100,
                'start': 0},
            proxies={'https': proxy})
