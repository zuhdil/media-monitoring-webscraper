import pytest
from webscraper.kaskus_driver import PageParser


@pytest.fixture
def first_page():
    with open('tests/fixtures/kaskus-search-first-page.html', 'r') as page:
        return PageParser(page.read())


@pytest.fixture
def last_page():
    with open('tests/fixtures/kaskus-search-last-page.html', 'r') as page:
        return PageParser(page.read())


def test_items_return_list_of_scrape_result_dic(first_page):
    assert len([item for item in first_page.items]) == 20


def test_result_item_title_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['title'] == (
            "'Om Telolet Om' Makan Korban Kaki Bocah Ini Patah"
            ' karena tertabrak gann')


def test_result_item_url_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['url'] == (
            'https://www.kaskus.co.id/thread/587349455a5163aa658b458a')


def test_result_item_snippet_has_correct_value(first_page):
    first_item = next(first_page.items)
    assert first_item['snippet'] == (
            "'Om Telolet Om' Makan Korban Kaki Bocah Ini Patah"
            ' karena tertabrak gann')


def test_is_last_on_first_page_should_return_false(first_page):
    assert first_page.is_last is False


def test_is_last_on_last_page_should_return_true(last_page):
    assert last_page.is_last is True
