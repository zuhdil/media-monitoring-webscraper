import pytest
from unittest.mock import Mock, PropertyMock
from requests import Session
from webscraper.kaskus_driver import PageRequest, PageParser


@pytest.fixture
def page_text():
    with open('tests/fixtures/kaskus-search-next-page.html') as page:
        return page.read()


@pytest.fixture
def session(page_text):
    session = Mock(Session)
    type(session.get.return_value).text = PropertyMock(return_value=page_text)
    return session


def test_should_make_get_request_with_kaskus_search_url(session):
    page_request = PageRequest(session, 'testing')
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.kaskus.co.id/search/forum',
            params={
                'q': 'testing',
                'date': 1,
                'sort': 'date',
                'order': 'desc'},
            proxies=None)


def test_should_include_page_params_if_page_greater_than_1(session):
    page_request = PageRequest(session, 'testing')
    page_request.get_page(2)
    session.get.assert_called_with(
            'https://www.kaskus.co.id/search/forum',
            params={
                'q': 'testing',
                'date': 1,
                'sort': 'date',
                'order': 'desc',
                'page': 2},
            proxies=None)


def test_get_page_should_return_PageParser_object(session):
    page_request = PageRequest(session, 'testing')
    page = page_request.get_page()
    assert type(page) == PageParser


def test_should_be_able_to_use_proxy_for_request_call(session):
    proxy = 'http://127.0.0.1:8080'
    page_request = PageRequest(session, 'testing', proxy=proxy)
    page_request.get_page()
    session.get.assert_called_with(
            'https://www.kaskus.co.id/search/forum',
            params={
                'q': 'testing',
                'date': 1,
                'sort': 'date',
                'order': 'desc'},
            proxies={'http': proxy})
