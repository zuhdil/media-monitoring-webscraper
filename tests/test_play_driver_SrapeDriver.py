import pytest
import json
from unittest.mock import Mock
from play_scraper.scraper import PlayScraper
from webscraper.play_driver import ScrapeDriver
from webscraper.scraper import FetchResult


@pytest.fixture
def search_result():
    with open('tests/fixtures/play-search-results.json', 'r') as file:
        return json.load(file)


@pytest.fixture
def scraper(search_result):
    scraper = Mock(PlayScraper)
    scraper.search.return_value = search_result
    return scraper


@pytest.fixture
def driver(scraper):
    return ScrapeDriver(scraper)


def test_should_return_list_of_FetchResult(driver):
    first_item = next(driver.fetch('testing'))
    assert (
            type(first_item) == FetchResult
            and first_item.source == driver.source)


def test_result_item_title_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.title == 'Speedtest.net'


def test_result_item_url_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.url == (
            'https://play.google.com/store/apps/details'
            '?id=org.zwanoo.android.speedtest')


def test_result_item_description_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.snippet == (
            'Ookla Speedtest, aplikasi #1 di dunia untuk menguji '
            'kecepatan Internet')


def test_should_be_callable(driver):
    items = [item for item in driver('testing')]
    assert len(items) == 20
