import pytest
from os import getenv
from random import choice
from uuid import uuid4
from datetime import datetime
from faker import Faker
from elasticsearch import Elasticsearch
from webscraper.scraper import ScrapeItem, Indexer


DOC_INDEX = getenv('SCRAPER_TEST_ELASTICSEARCH_INDEX', 'brand-monitoring-test')
DOC_TYPE = 'snippets'
DOC_MAPPING = {
        "mappings": {
            "snippets": {
                "properties": {
                    "category": {"type": "keyword"},
                    "id": {"type": "keyword"},
                    "owner": {"type": "keyword"},
                    "snippet": {
                        "type": "text",
                        "fields": {
                            "ind": {"type": "text", "analyzer": "indonesian"},
                            "raw": {"type": "keyword"}
                            }
                        },
                    "source": {"type": "keyword"},
                    "timestamp": {"type": "date"},
                    "title": {
                        "type": "text",
                        "fields": {
                            "ind": {"type": "text", "analyzer": "indonesian"},
                            "raw": {"type": "keyword"}
                            }
                        },
                    "url": {
                        "type": "text",
                        "fields": {"raw": {"type": "keyword"}}
                        }
                    }
                }
            }
        }
fake = Faker()


class ScrapeItemFixture:
    def __init__(
            self, title=None, url=None, snippet=None, source=None,
            owner=None, id=None, timestamp=None):
        self.title = title if title else fake.sentence()
        self.url = url if url else fake.uri()
        self.snippet = snippet if snippet else fake.paragraph()
        self.source = source if source else choice([
            'www.google.com', 'www.bing.com', 'twitter.com'])
        self.owner = owner if owner else uuid4()
        self.id = id
        self.timestamp = timestamp.isoformat() \
            if isinstance(timestamp, datetime) \
            else timestamp

    def build(self):
        return ScrapeItem(
                self.title, self.url, self.snippet, self.source,
                self.owner, self.id, self.timestamp)


class ElasticsearchTestHelper:
    def __init__(self, client, index, type):
        self.client = client
        self.index = index
        self.type = type

    def search(self, body):
        self.refresh()
        response = self.client.search(
                index=self.index, doc_type=self.type, body=body)
        return response['hits']

    def refresh(self):
        self.client.indices.refresh(index=self.index)


@pytest.fixture
def elasticsearch():
    es = Elasticsearch()
    if (es.indices.exists(index=DOC_INDEX)):
        es.indices.delete(index=DOC_INDEX)

    es.indices.create(index=DOC_INDEX, body=DOC_MAPPING)

    yield es

    if (es.indices.exists(index=DOC_INDEX)):
        es.indices.delete(index=DOC_INDEX)


@pytest.fixture
def client(elasticsearch):
    return ElasticsearchTestHelper(elasticsearch, DOC_INDEX, DOC_TYPE)


@pytest.fixture
def indexer(elasticsearch):
    return Indexer(elasticsearch, DOC_INDEX, DOC_TYPE)


def test_indexing_snippet(client, indexer):
    item = ScrapeItemFixture().build()
    indexer.index(item)
    result = client.search({'query': {'match': {'id': str(item.id)}}})
    assert result['hits'][0]['_source']['title'] == item.title


def test_not_indexing_the_same_snippet(client, indexer):
    item = ScrapeItemFixture().build()
    indexer.index(item)
    duplicate = ScrapeItemFixture(
            title=item.title,
            snippet=item.snippet,
            url=item.url,
            source=item.source,
            owner=item.owner).build()
    indexer.index(duplicate)
    result = client.search({'query': {'match': {'id': str(duplicate.id)}}})
    assert result['total'] == 0


def test_indexer_is_callable(client, indexer):
    item = ScrapeItemFixture().build()
    indexer(item)
    result = client.search({'query': {'match': {'id': str(item.id)}}})
    assert result['hits'][0]['_source']['title'] == item.title
