from uuid import UUID, uuid4
from datetime import datetime
from webscraper.scraper import ScrapeItem


def test_create_item_without_id_should_generate_id():
    item = ScrapeItem(
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux')
    assert type(item.id) is UUID


def test_should_be_able_to_create_item_with_existing_id():
    id = uuid4()
    item = ScrapeItem(
            id=str(id),
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux')
    assert item.id == id


def test_create_item_without_timestamp_should_self_stamp_time():
    item = ScrapeItem(
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux')
    assert type(item.timestamp) is datetime


def test_create_item_with_timestamp():
    a_time = datetime.now()
    item = ScrapeItem(
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux',
            timestamp=a_time)
    assert item.timestamp.strftime('%Y-%m-%dT%H:%I:%S') == (
            a_time.strftime('%Y-%m-%dT%H:%I:%S'))


def test_item_equality():
    id = str(uuid4())
    timestamp = datetime.now()
    a = ScrapeItem(
            id=id,
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux',
            timestamp=timestamp)
    b = ScrapeItem(
            id=id,
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            owner='baz',
            source='quux',
            timestamp=timestamp)
    assert a == b


def test_convert_item_to_dict():
    item = ScrapeItem(
            title='foo',
            url='http://f.oo',
            snippet='foo bar',
            owner='owner',
            source='source')
    assert item.to_dict() == {
            'id': item.id,
            'title': item.title,
            'url': item.url,
            'snippet': item.snippet,
            'owner': item.owner,
            'source': item.source,
            'timestamp': item.timestamp}
