import pytest
from unittest.mock import Mock, call
from webscraper.scraper import Scraper, ScrapeItem, FetchResult


class ScraperFixture():
    def __init__(self):
        self.owner = 'owner'
        self.indexer = Mock()
        self.drivers = [Mock(return_value=[])]

    def build(self, owner=None, drivers=None, indexer=None):
        if owner:
            self.owner = owner
        if drivers:
            self.drivers = drivers
        if indexer:
            self.indexer = indexer
        return Scraper(self.owner, self.drivers, self.indexer)


@pytest.fixture
def scraper_builder():
    return ScraperFixture()


def test_scraper_does_not_index_if_drivers_returns_empty_result(
        scraper_builder):
    scraper = scraper_builder.build()
    scraper.scrape('bar')
    assert not scraper_builder.indexer.called


def test_scraper_should_call_source_scraper_with_query_term(scraper_builder):
    driver = Mock(return_value=[])
    scraper = scraper_builder.build(drivers=[driver])
    query = 'foo bar'
    scraper.scrape(query)
    assert driver.call_args == call(query)


def test_scraper_should_index_result_scrape_item(scraper_builder):
    result = FetchResult(
            title='foo',
            url='http://example.com',
            snippet='foo bar',
            source='source-1')
    scraper = scraper_builder.build(
            drivers=[Mock(return_value=[result])])
    scraper.scrape('bar')
    (actual,), _ = scraper_builder.indexer.call_args
    assert (
            type(actual) == ScrapeItem
            and actual.title == result.title
            and actual.url == result.url
            and actual.snippet == result.snippet
            and actual.source == 'source-1'
            and actual.owner == scraper_builder.owner)


def test_scraper_should_index_all_result_from_all_drivers(scraper_builder):
    result1 = FetchResult(
            title='foo',
            url='http://foo.oo',
            snippet='foo oo',
            source='source-1')
    result2 = FetchResult(
            title='bar',
            url='http://bar.rr',
            snippet='bar rr',
            source='source-2')
    scraper = scraper_builder.build(
            drivers=[
                Mock(return_value=[result1]),
                Mock(return_value=[result2])])
    scraper.scrape('bar')
    assert scraper_builder.indexer.call_count == 2
