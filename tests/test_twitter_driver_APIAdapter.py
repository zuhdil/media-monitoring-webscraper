from os import getenv
from tweepy.models import Status
from vcr import VCR
from webscraper.twitter_driver import TwitterAPI


use_replay = getenv('USE_REPLAY', False)
tape = VCR(
    cassette_library_dir='tests/fixtures',
    filter_headers=['Authorization'],
    serializer='json',
    # Either use existing cassettes, or never use recordings:
    record_mode='all' if use_replay else 'none')


@tape.use_cassette('test_search_return_iterable_statuses_cassette.json')
def test_search_return_iterable_statuses():
    api = TwitterAPI(
            getenv('TWITTER_CONSUMER_KEY'), getenv('TWITTER_CONSUMER_SECRET'))
    first_item = next(api.search('testing'))
    assert isinstance(first_item, Status)
