import pytest
from unittest.mock import Mock
from tweepy.parsers import ModelParser
from webscraper.twitter_driver import ScrapeDriver, TwitterAPI
from webscraper.scraper import FetchResult


class FakeAPI:
    def __init__(self, parser):
        self.parser = parser


class FakeSession:
    params = {}


class FakeMethod:
    payload_type = 'search_results'
    payload_list = False
    session = FakeSession()

    def __init__(self, api):
        self.api = api


@pytest.fixture
def search_results():
    with open('tests/fixtures/twitter-search-results.json', 'r') as results:
        return results.read()


@pytest.fixture
def api(search_results):
    api = Mock(TwitterAPI)
    parser = ModelParser()
    method = FakeMethod(FakeAPI(parser))
    results = parser.parse(method, search_results)
    api.search.return_value = iter(results)
    return api


@pytest.fixture
def driver(api):
    return ScrapeDriver(api)


def test_should_return_list_of_FetchResult(driver):
    first_item = next(driver.fetch('testing'))
    assert (
            type(first_item) == FetchResult
            and first_item.source == driver.source)


def test_result_item_title_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.title == (
            "@Emploi_Finance : Emploi Murex : Consultant Fonctionnel "
            "Operations et Finance.+ 3e annee d'ecole...")


def test_result_item_url_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.url == (
            'https://twitter.com/Emploi_Finance/status/805215475898732544')


def test_result_item_description_should_have_correct_value(driver):
    first_item = next(driver.fetch('testing'))
    assert first_item.snippet == (
            "Emploi Murex : Consultant Fonctionnel Operations et Finance.+ 3e "
            "annee d'ecole d'in... https://t.co/v2td3HVyTC Emploi Finance 51")


def test_should_be_callable(driver):
    items = [item for item in driver('testing')]
    assert len(items) == 99
