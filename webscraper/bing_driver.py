import re
import time
from lxml import html, etree
from .scraper import FetchResult


def extract_snippet(element):
    element_str = str(etree.tostring(element), 'utf-8')
    cleaned = re.sub(
            r'<span class="news_dt">.+</span>&#\d+;&#\d+;\ ', '', element_str)
    return str(html.fromstring(cleaned).text_content())


class PageParser:
    def __init__(self, content):
        self.doc = html.fromstring(content)

    @property
    def items(self):
        for li in self.doc.cssselect('#b_results .b_algo'):
            heading = li.cssselect('h2 a')[0]
            title = heading.text_content()
            url = heading.attrib['href']
            body = li.cssselect('.b_caption p')[0]
            snippet = extract_snippet(body)
            yield {'title': title, 'url': url, 'snippet': snippet}

    @property
    def is_last(self):
        elements = self.doc.cssselect('#b_results .b_pag a.sb_pagN')
        return False if len(elements) else True


class PageRequest:
    def __init__(self, session, query, perpage=100, proxy=None):
        self.session = session
        self.query = query
        self.perpage = perpage if perpage <= 100 and perpage > 0 else 100
        self.proxies = {'http': proxy} if proxy else None

    def get_page(self, page=1):
        response = self.session.get(
                'https://www.bing.com/search',
                params={
                    'q': self.query,
                    'filters': 'ex1:"ez1"',
                    'count': self.perpage,
                    'first': self.perpage * (page - 1),
                    'FORM': 'PERE'},
                proxies=self.proxies)
        return PageParser(response.text)


class ScrapeDriver:
    source = 'www.bing.com'

    def __init__(self, session, sleep=5.0, proxy=None):
        self.session = session
        self.sleep = sleep
        self.proxy = proxy

    def fetch(self, query, perpage=100):
        pagenum = 1
        stop = False
        request = PageRequest(self.session, query, perpage, self.proxy)
        while not stop and pagenum <= 5:
            page = request.get_page(pagenum)
            for item in page.items:
                yield FetchResult(
                        title=item['title'],
                        url=item['url'],
                        snippet=item['snippet'],
                        source=self.source)
            pagenum = pagenum + 1
            stop = page.is_last
            if not stop:
                time.sleep(self.sleep)

    def __call__(self, query, perpage=100):
        return self.fetch(query, perpage)
