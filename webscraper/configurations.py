from sqlalchemy import (
        Column, CHAR, String, DateTime, Boolean, ForeignKey,
        func, and_, true, null)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Client(Base):
    __tablename__ = 'clients'

    id = Column(CHAR(36), primary_key=True)
    name = Column(String)
    username = Column(String)
    password = Column(String)
    remember_token = Column(String(100))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    is_active = Column(Boolean)

    monitoring_configs = relationship(
            'MonitoringConfig', back_populates='client')


class MonitoringConfig(Base):
    __tablename__ = 'monitoring_configs'

    id = Column(CHAR(36), primary_key=True)
    client_id = Column(CHAR(36), ForeignKey('clients.id'))
    keyword = Column(String)
    started_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    client = relationship('Client', back_populates='monitoring_configs')


class ConfigRepository:
    def __init__(self, session):
        self.session = session

    def get_active(self, on_date):
        subsql = self.session.query(
                    MonitoringConfig.client_id,
                    func.max(
                        MonitoringConfig.started_at
                    ).label('effective_date')
                ).filter(
                    MonitoringConfig.started_at <= on_date
                ).group_by(
                    MonitoringConfig.client_id
                ).subquery()

        stmt = self.session.query(MonitoringConfig).outerjoin(
                subsql,
                and_(
                    MonitoringConfig.client_id == subsql.c.client_id,
                    MonitoringConfig.started_at == subsql.c.effective_date
                )).join(Client).filter(
                        and_(
                            subsql.c.effective_date != null(),
                            Client.is_active == true()))

        return stmt.all()
