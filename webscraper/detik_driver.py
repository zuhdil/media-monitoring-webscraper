import re
import time
from lxml import html
from datetime import datetime
from .scraper import FetchResult, TZ


_SNIPPET_META = r"\b\w+,\s+\d{2}\/\d{2}\/\d{4}\s+\d{2}:\d{2}\s+WIB[\s\w\W]+$"


def extract_snippet(element, title):
    text = element.text_content().replace(title, '').strip()
    snippet = re.sub(_SNIPPET_META, '', text, 0, re.MULTILINE)
    return snippet.strip()


class PageParser:
    def __init__(self, content):
        self.doc = html.fromstring(content)
        self._items = []
        self._populate()

    def _populate(self):
        for el in self.doc.cssselect('.news-list > li'):
            title_el = el.cssselect('.title a')
            if len(title_el) == 1:
                title = title_el[0].text_content().strip()
                url = title_el[0].attrib['href']
                snippet = extract_snippet(el, title)
                self._items.append({
                    'title': title,
                    'url': url,
                    'snippet': snippet})

    @property
    def items(self):
        for item in self._items:
            yield item

    @property
    def is_last(self):
        return len(self._items) < 10


class PageRequest:
    def __init__(self, session, query, date=None, proxy=None):
        self.session = session
        self.query = query
        self.date = date if isinstance(date, datetime) else datetime.now(TZ)
        self.proxies = {'http': proxy} if proxy else None

    def get_page(self, page=1):
        params = {
                'query': self.query,
                'sortby': 'time',
                'fromdatex': self.date.strftime('%d/%m/%Y'),
                'todatex': self.date.strftime('%d/%m/%Y')}
        if page > 1:
            params['page'] = page

        response = self.session.get(
                'http://search.detik.com/search',
                params=params,
                proxies=self.proxies)

        return PageParser(response.text.encode('utf-8'))


class ScrapeDriver:
    source = 'www.detik.com'

    def __init__(self, session, sleep=5.0, date=None, proxy=None):
        self.session = session
        self.sleep = sleep
        self.date = date
        self.proxy = proxy

    def fetch(self, query):
        pagenum = 1
        stop = False
        request = PageRequest(
                self.session, query, date=self.date, proxy=self.proxy)
        while not stop:
            page = request.get_page(pagenum)
            for item in page.items:
                yield FetchResult(
                        title=item['title'],
                        url=item['url'],
                        snippet=item['snippet'],
                        source=self.source)
            stop = page.is_last
            pagenum = pagenum + 1
            if not stop:
                time.sleep(self.sleep)

    def __call__(self, query):
        return self.fetch(query)
