import logging
import pickle
import json
from os.path import isfile
from datetime import datetime
from urllib.parse import urlencode, urljoin
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import (
        text_to_be_present_in_element, visibility_of_element_located)
from selenium.common.exceptions import TimeoutException, WebDriverException
from .pageobject import PageObject, WrapElement, Find, FindAll
from .scraper import FetchResult, TZ


DEFAULT_TIMEOUT = 10
DEFAULT_MAX_SCROLL = 10

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class LoginFailureError(RuntimeError):
    pass


class LoginPage(PageObject):
    email = Find(id='email')
    password = Find(id='pass')
    login_btn = Find(css='#login_form input[type=submit]')
    logged_in = Find(css='input[name=q]')

    def __init__(self, driver, url, cookies_file):
        super().__init__(driver, url)
        self.cookies_file = cookies_file
        self.wait = WebDriverWait(self.driver, DEFAULT_TIMEOUT)

    def load_session(self, email, password):
        if not isfile(self.cookies_file):
            self.login(email, password)
        else:
            cookies = pickle.load(open(self.cookies_file, 'rb'))
            for cookie in cookies:
                self.driver.add_cookie(cookie)

            self.open()

        if not self.is_logged_in():
            self.force_login(email, password)

        if not self.is_logged_in():
            raise LoginFailureError(email)

        return HomePage(self.driver, self.driver.current_url)

    def is_logged_in(self):
        return self.logged_in is not None

    def login(self, email, password):
        self.wait_form_ready()
        self.email.send_keys(email)
        self.password.send_keys(password)
        self.login_btn.click()

        if self.is_logged_in():
            cookies = self.driver.get_cookies()
            pickle.dump(cookies, open(self.cookies_file, 'wb'))

    def force_login(self, email, password):
        counter = 0
        while not self.is_logged_in() and counter < 3:
            counter = counter + 1
            self.driver.delete_all_cookies()
            self.open()
            self.login(email, password)

    def wait_form_ready(self):
        self.wait.until(visibility_of_element_located(
            type(self).login_btn.locator))


class HomePage(PageObject):

    def search(self, query, start, end, max_scroll=DEFAULT_MAX_SCROLL):
        filter = json.dumps({
                    'start_month': start.strftime('%Y-%m'),
                    'end_month': end.strftime('%Y-%m')},
                separators=(',', ':'))
        query_str = urlencode({'q': query, 'filters_rp_creation_time': filter})
        search_url = urljoin(self.url, '/search/top/?{}'.format(query_str))

        return SearchResultsPage(self.driver, search_url, max_scroll).open()


class ResultContent(WrapElement):
    title_container = Find(tag_name='h5')
    time_container = Find(css='h5+* a>abbr')
    snippet_container = Find(css='.userContent')

    def __init__(self, element):
        super().__init__(element)
        self._title = None
        self._datetime = None
        self._url = None
        self._snippet = None

    @property
    def title(self):
        return self._title if self._title else self.title_container.text

    @property
    def datetime(self):
        if not self._datetime:
            self._datetime = datetime.fromtimestamp(
                    int(self.time_container.get_attribute('data-utime')), TZ)

        return self._datetime

    @property
    def url(self):
        if not self._url:
            link_el = self.time_container.get_property('parentElement')
            self._url = link_el.get_attribute('href')

        return self._url

    @property
    def snippet(self):
        return self._snippet if self._snippet else self.snippet_container.text

    def to_dict(self):
        return {
                'title': self.title,
                'datetime': self.datetime,
                'url': self.url,
                'snippet': self.snippet}


class ResultArea(WrapElement):
    contents = FindAll(ResultContent, css='.fbUserContent')


class SearchResultsPage(PageObject):
    result_area = Find(ResultArea, id='browse_result_area')
    end_of_results = Find(id='browse_end_of_results_footer')

    def __init__(self, driver, url, max_scroll=DEFAULT_MAX_SCROLL):
        super().__init__(driver, url)
        self.max_scroll = max_scroll
        self.wait = WebDriverWait(self.driver, DEFAULT_TIMEOUT)

    def open(self):
        super().open()

        counter = 0
        scroll_script = 'window.scrollTo(0, document.body.scrollHeight);'
        try:
            while not self.end_of_results and counter < self.max_scroll:
                self.wait_results_loaded()
                counter = counter + 1
                self.driver.execute_script(scroll_script)
        except TimeoutException:
            logger.info((
                'timeout while scrolling, '
                'just index what we got so far..'))

        return self

    def wait_results_loaded(self):
        self.wait.until_not(text_to_be_present_in_element(
            type(self).result_area.locator, 'Loading more results...'))

    def get_results(self):
        for content in self.result_area.contents:
            try:
                yield content.to_dict()
            except (WebDriverException, AttributeError):
                logger.info('bypassed non standard content structure')


class ScrapeDriver:
    source = 'www.facebook.com'

    def __init__(self, driver, user, password, cookies_file='cookies.pkl'):
        self.driver = driver
        self.user = user
        self.password = password
        self.cookies_file = cookies_file

    def fetch(self, query, max_scroll=DEFAULT_MAX_SCROLL):
        url = 'https://' + self.source
        login = LoginPage(self.driver, url, self.cookies_file)
        home = login.open().load_session(self.user, self.password)
        date_now = datetime.now()
        search = home.search(query, date_now, date_now, max_scroll)

        for item in search.get_results():
            item_link = item['url'] if item['url'].find(url) >= 0 else url + '/' + item['url'].lstrip('/')
            yield FetchResult(
                    title=item['title'],
                    url=item_link,
                    snippet=item['snippet'],
                    source=self.source)

    def __call__(self, query, max_scroll=DEFAULT_MAX_SCROLL):
        return self.fetch(query, max_scroll)

    def close(self):
        self.driver.quit()
