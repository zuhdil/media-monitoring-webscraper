import re
import time
from urllib import parse
from lxml import html
from .scraper import FetchResult


def extract_url(url):
    match = re.match(r'/url\?q=(http[^&]+)&', url)
    return match.group(1) if match else url


class PageParser:
    def __init__(self, content):
        self.doc = html.fromstring(content)

    @property
    def items(self):
        for div in self.doc.cssselect('#search .g'):
            heading = div.cssselect('h3.r a')[0]
            title = heading.text_content()
            url = parse.unquote(extract_url(heading.attrib['href']))
            snippet = ' '.join(
                    [it.text_content() for it in div.cssselect('.s .st')])
            yield {'title': title, 'url': url, 'snippet': snippet}

    @property
    def is_last(self):
        container = self.doc.cssselect('#nav tr td:last-child a')
        return False if len(container) else True


class PageRequest:
    def __init__(self, session, query, perpage=100, proxy=None):
        self.session = session
        self.query = query
        self.perpage = perpage if perpage <= 100 and perpage > 0 else 100
        self.proxies = {'https': proxy} if proxy else None

    def get_page(self, page=1):
        offset = self.perpage * (page - 1)
        response = self.session.get(
                'https://www.google.com/search',
                params={
                    'q': self.query,
                    'hl': 'id',
                    'btnG': 'Google Search',
                    'tbs': 'qdr:h',
                    'safe': 'off',
                    'num': self.perpage,
                    'start': offset},
                proxies=self.proxies)
        return PageParser(response.text)


class ScrapeDriver:
    source = 'www.google.com'

    def __init__(self, session, sleep=5.0, proxy=None):
        self.session = session
        self.sleep = sleep
        self.proxy = proxy

    def fetch(self, query, perpage=100):
        pagenum = 1
        stop = False
        request = PageRequest(self.session, query, perpage, self.proxy)
        while not stop:
            page = request.get_page(pagenum)
            for item in page.items:
                yield FetchResult(
                        title=item['title'],
                        url=item['url'],
                        snippet=item['snippet'],
                        source=self.source)
            pagenum = pagenum + 1
            stop = page.is_last
            if not stop:
                time.sleep(self.sleep)

    def __call__(self, query, perpage=100):
        return self.fetch(query, perpage)
