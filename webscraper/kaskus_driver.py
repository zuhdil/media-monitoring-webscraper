import time
from lxml import html
from .scraper import FetchResult


class PageParser:
    def __init__(self, content):
        self.doc = html.fromstring(content)

    @property
    def items(self):
        for el in self.doc.cssselect('#forum-search-result .post'):
            title_el = el.cssselect('.post-title a')[0]
            title = title_el.text_content().strip()
            url = title_el.attrib['href']
            yield {'title': title, 'url': url, 'snippet': title}

    @property
    def is_last(self):
        el = self.doc.cssselect(
                '#forum-search-result .pagination '
                'a[data-original-title="Next Page"]')
        return False if len(el) else True


class PageRequest:
    def __init__(self, session, query, proxy=None):
        self.session = session
        self.query = query
        self.proxies = {'http': proxy} if proxy else None

    def get_page(self, page=1):
        params = {
                'q': self.query,
                'date': 1,
                'sort': 'date',
                'order': 'desc'}
        if page > 1:
            params['page'] = page

        response = self.session.get(
                'https://www.kaskus.co.id/search/forum',
                params=params,
                proxies=self.proxies)

        return PageParser(response.text)


class ScrapeDriver:
    source = 'www.kaskus.co.id'

    def __init__(self, session, sleep=5.0, proxy=None):
        self.session = session
        self.proxy = proxy
        self.sleep = sleep

    def fetch(self, query):
        pagenum = 1
        stop = False
        request = PageRequest(self.session, query, self.proxy)
        while not stop:
            page = request.get_page(pagenum)
            for item in page.items:
                yield FetchResult(
                        title=item['title'],
                        url=item['url'],
                        snippet=item['snippet'],
                        source=self.source)
            stop = page.is_last
            pagenum = pagenum + 1
            if not stop:
                time.sleep(self.sleep)

    def __call__(self, query):
        return self.fetch(query)
