from urllib.parse import urljoin
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import NoSuchElementException


class PageObject:
    def __init__(self, driver, url=None):
        self._driver = driver
        self._url = url if url else getattr(self, 'URL', None)

    @property
    def url(self):
        return self._url

    @property
    def driver(self):
        return self._driver

    def open(self):
        self._driver.get(self.url)
        return self

    def goto(self, path):
        self._driver.get(urljoin(self.url, path))
        return PageObject(self._driver, self._driver.current_url)

    def find_element(self, *args):
        return self._driver.find_element(*args)

    def find_elements(self, *args):
        return self._driver.find_elements(*args)


class WrapElement:
    def __init__(self, element):
        if not isinstance(element, WebElement):
            raise ValueError('element must be instance of WebElement')
        self._inner = element

    def __getattr__(self, attr):
        return getattr(self._inner, attr)


LOCATOR_MAP = {
        'css': By.CSS_SELECTOR,
        'id': By.ID,
        'name': By.NAME,
        'xpath': By.XPATH,
        'link_text': By.LINK_TEXT,
        'partial_link_text': By.PARTIAL_LINK_TEXT,
        'tag_name': By.TAG_NAME,
        'class_name': By.CLASS_NAME}


class Find:
    def __init__(self, wrap=None, **kwargs):
        if not kwargs:
            raise ValueError('Please specify a locator')
        if len(kwargs) > 1:
            raise ValueError('Please specify only one locator')
        if wrap and not issubclass(wrap, WrapElement):
            raise ValueError('wrap types should inherit WrapElement')

        k, v = next(iter(kwargs.items()))
        if k not in LOCATOR_MAP:
            raise ValueError('Invalid locator "{}"'.format(k))

        self.locator = (LOCATOR_MAP[k], v)
        self.wrap = wrap

    def __get__(self, context, _):
        if not context:
            return self

        return self._find(context)

    def _find(self, context):
        try:
            el = context.find_element(*self.locator)
            return el if not self.wrap else self.wrap(el)
        except NoSuchElementException:
            return None


class FindAll(Find):
    def _find(self, context):
        try:
            els = context.find_elements(*self.locator)
            return els if not self.wrap else [self.wrap(el) for el in els]
        except NoSuchElementException:
            return []
