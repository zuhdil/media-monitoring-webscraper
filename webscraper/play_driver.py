from .scraper import FetchResult


class ScrapeDriver:
    source = 'play.google.com'

    def __init__(self, scraper):
        self.scraper = scraper

    def fetch(self, query):
        for item in self.scraper.search(query, page=12):
            yield FetchResult(
                    title=item['title'],
                    url=item['url'],
                    snippet=item['description'],
                    source=self.source)

    def __call__(self, query):
        return self.fetch(query)
