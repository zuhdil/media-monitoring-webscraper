from uuid import UUID, uuid4
from datetime import datetime, timezone, timedelta


TZ = timezone(timedelta(hours=7))


class FetchResult:
    def __init__(self, title, url, snippet, source):
        self.title = title
        self.url = url
        self.snippet = snippet
        self.source = source


class ScrapeItem:
    def __init__(
            self, title, url, snippet, source, owner,
            id=None, timestamp=None):
        self.id = UUID(id) if id else uuid4()
        self.title = title
        self.url = url
        self.snippet = snippet
        self.owner = owner
        self.source = source
        self.timestamp = (
                timestamp
                if isinstance(timestamp, datetime)
                else datetime.now(TZ))

    def to_dict(self):
        return self.__dict__

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class Scraper:
    def __init__(self, owner, drivers, indexer):
        self.owner = owner
        self.drivers = drivers
        self.index = indexer

    def scrape(self, query):
        for scrape in self.drivers:
            for each in scrape(query):
                item = ScrapeItem(
                        title=each.title,
                        url=each.url,
                        snippet=each.snippet,
                        source=each.source,
                        owner=self.owner)
                self.index(item)


class Indexer:
    def __init__(self, client, index, type='snippets'):
        self.client = client
        self.doc_index = index
        self.doc_type = type

    def index(self, item):
        if not self.is_duplicate(item):
            self.client.index(
                    index=self.doc_index, doc_type=self.doc_type,
                    body=item.to_dict(), id=item.id)

    def is_duplicate(self, item):
        self.client.indices.refresh(index=self.doc_index)
        body = {
                "query": {
                    "bool": {
                        "must": [
                            {"term": {"title.raw": {"value": item.title}}},
                            {"term": {"snippet.raw": {"value": item.snippet}}},
                            {"term": {"url.raw": {"value": item.url}}},
                            {"term": {"source": {"value": item.source}}},
                            {"term": {"owner": {"value": item.owner}}}
                            ]
                        }
                    }
                }
        response = self.client.search(
                index=self.doc_index, doc_type=self.doc_type, body=body)
        return response['hits']['total'] > 0

    def __call__(self, item):
        return self.index(item)
