from .scraper import FetchResult
from tweepy import API, OAuthHandler, Cursor
from textwrap import shorten


class TwitterAPI:
    def __init__(self, consumer_key, consumer_secret, proxy=None):
        auth = OAuthHandler(consumer_key, consumer_secret)
        self.api = API(auth, proxy=proxy)

    def search(self, q):
        for item in Cursor(self.api.search, q=q).items():
            yield item


class ScrapeDriver:
    source = 'twitter.com'
    url = 'https://twitter.com'

    def __init__(self, api):
        self.api = api

    def fetch(self, query):
        for item in self.api.search(query):
            yield FetchResult(
                    title=shorten(
                        "@{user} : {tweet}".format(
                            user=item.user.screen_name, tweet=item.text),
                        width=100,
                        placeholder="..."),
                    url="{host}/{user}/status/{id}".format(
                        host=self.url, user=item.user.screen_name, id=item.id),
                    snippet=item.text,
                    source=self.source)

    def __call__(self, query):
        return self.fetch(query)
